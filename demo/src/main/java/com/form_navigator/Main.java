package com.form_navigator;

import com.form_navigator.controller.FormNavigatorApp;

import javafx.application.Application;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");
        Application.launch(FormNavigatorApp.class, args);
    }
}